# Description:
#   Showme shows you things
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   worf show me pug - get a pug
#   worf show me cat - get a cat
#   worf show me 5 pugs - get 5 pugs
#   worf fluffy dog - get a fluffy dog
#   worf 5 fluffy dogs - get 5 fluffy dogs
#   worf goose - get a goose
#   worf 5 goose - get 5 geese
#   worf geese - get a goose
#   worf 5 geese - get 5 geese
module.exports = (robot) ->
  WORF_COGNITIVE_TOKEN = process.env.WORF_COGNITIVE_TOKEN
  responseFunction = (count, query, msg, limit = false) ->
    url = "https://api.cognitive.microsoft.com/bing/v5.0/images/search?q=" + encodeURIComponent(query) + "&mkt=en-us"
    console.log(url)
    count = Math.min(count, 50)
    top = 50
    offset = if limit then 0 else Math.floor(Math.random() * 20) * 50
    requestUrl = url + "&offset=" + offset + "&count=" + top
    console.log(requestUrl)
    console.log(count)
    msg.http(requestUrl)
      .headers(Accept: 'application/json', "Ocp-Apim-Subscription-Key": WORF_COGNITIVE_TOKEN)
      .get() (err, res, body) ->
        switch res.statusCode
          when 200
            results = JSON.parse(body).value
            if (results.length > count && !limit)
              results = results.slice(0, count)
            else
              sliceIndex = Math.floor(Math.random() * 20)
              results = results.slice(sliceIndex, sliceIndex + 1)
            console.log(results.length)
            for entry in results
              console.log(entry.contentUrl)
              msg.send entry.contentUrl 
          else
            console.log(err)
            console.log(body)
            msg.send "NO DOGS FOR YOU"  


  robot.respond /fluffy dog/i, (msg) ->
    responseFunction(1, "Fluffy Dog", msg)    

  robot.respond /([0-9]+) fluffy dogs/i, (msg) ->
    responseFunction(msg.match[1], "Fluffy Dog", msg)    

  robot.respond /goose/i, (msg) ->
    responseFunction(1, "Goose Bird", msg) 

  robot.respond /geese/i, (msg) ->
    responseFunction(1, "Geese Bird", msg) 

  robot.respond /([0-9]+) goose/i, (msg) ->
    responseFunction(msg.match[1], "Goose Bird", msg) 

  robot.respond /([0-9]+) geese/i, (msg) ->
    responseFunction(msg.match[1], "Geese Bird", msg) 

  robot.respond /ninja goose/i, (msg) ->
    responseFunction(1, "Goose Bird", msg) 

  robot.respond /([0-9]+) ninja goose/i, (msg) ->
    responseFunction(msg.match[1], "Goose Bird", msg) 

  robot.respond /ninja geese/i, (msg) ->
    responseFunction(1, "Geese Bird", msg) 

  robot.respond /([0-9]+) ninja geese/i, (msg) ->
    responseFunction(msg.match[1], "Geese Bird", msg)

  robot.respond /love cactus/i, (msg) ->
    responseFunction(1, "Love Cactus Plant", msg)

  robot.respond /([0-9]+) love cactus/i, (msg) ->
    responseFunction(msg.match[1], "Love Cactus Plant", msg)

  robot.respond /([0-9]+) love cacti/i, (msg) ->
    responseFunction(msg.match[1], "Love Cactus Plant", msg)

  robot.respond /chubbs/i, (msg) ->
    responseFunction(1, "Chubbs Peterson", msg, true) 

  robot.respond /show me ([0-9]+) (.*)/i, (msg) ->
    responseFunction(msg.match[1], msg.match[2], msg)   

  robot.respond /show me ([a-zA-Z]+)/i, (msg) ->
    responseFunction(1, msg.match[2], msg)

  robot.respond /you're a dog/i, (msg) ->
    msg.send "https://www.youtube.com/watch?v=-xw2H_lkQaw"
  
  robot.respond /(.*) a goose/i, (msg) ->
    msg.send "https://www.youtube.com/watch?v=7zMItQv7ZH8"
