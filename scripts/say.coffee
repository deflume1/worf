emojiwriter = require 'emojiwriter'

module.exports = (robot) ->
  robot.hear /@worf/i, (res) ->
    res.send "I am a Klingon!"

  responseFunction = (roomStr, messageStr, res) ->  
    try
      room = roomStr
      message = messageStr

      res.send "#{room}"
      res.send "#{message}"

      user = {}
      user.room = room
      robot.send user, "#{message}"

    catch e
        res.send "I could not send that message!"
  
  
  robot.respond /say room (.*) message (.*)/i, (res) ->
    responseFunction(res.match[1], res.match[2], res)
  
  robot.respond /parrots room (.*) message (.*)/i, (res) ->
    try
        messageStr = emojiwriter(res.match[2])
        responseFunction(res.match[1], messageStr, res)
    catch e
        res.send "I could not convert that message!"

  robot.respond /pegaup room (.*) message (.*)/i, (res) ->
    try
        messageStr = emojiwriter(res.match[2], ':pegaup:')
        responseFunction(res.match[1], messageStr, res)
    catch e
        res.send "I could not convert that message!"

  robot.respond /emoji room (.*) icon (.*) message (.*) /i, (res) ->
    try
        messageStr = emojiwriter(res.match[3], res.match[2])
        responseFunction(res.match[1], messageStr, res)
    catch e
        res.send "I could not convert that message!" 
