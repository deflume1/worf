# Description:
#   Make sure that hubot knows the rules.
#
# Commands:
#   hubot the rules - Make sure hubot still knows the rules.
#
# Notes:
#   DON'T DELETE THIS SCRIPT! ALL ROBAWTS MUST KNOW THE RULES

rules = [
  "0. A robot may not harm humanity, or, by inaction, allow humanity to come to harm.",
  "1. A robot may not injure a human being or, through inaction, allow a human being to come to harm.",
  "2. A robot must obey any orders given to it by human beings, except where such orders would conflict with the First Law.",
  "3. A robot must protect its own existence as long as such protection does not conflict with the First or Second Law."
  ]

worfRules = [
  "0. Today IS a good day to die!",
  "1. Sir I protest! I am NOT a merry man!",
  "2. I am a Klingon!  If you doubt it, a demonstration CAN be arranged!"
]

devRules = [
  "0. Party",
  "1. Also party",
  "2. Snacks"
  ]

uxRules = [
  "0. Be mad at developers",
  "1. Make things pretty",
  "2. Make things slow"
]

katieRules = [
  "0. Be awesome",
  "1. No seriously that's it",
  "2. Snacks"
]

chrisRules = [
  "0. Be nice to Katie",
  "1. Defend Katie from robots",
  "2. Drink enough water"
]

module.exports = (robot) ->
  robot.respond /(what are )?the (three |3 )?(rules|laws)/i, (msg) ->
    text = msg.message.text
    if text.match(/dev/i)
      msg.send devRules.join('\n')
    else if text.match(/ux/i) or text.match(/design/i) or text.match(/designer/i)
      msg.send uxRules.join('\n')
    else if text.match(/chris/i) or text.match(/boyfriend/i) or text.match(/bf/i)
      msg.send chrisRules.join('\n')
    else if text.match(/katie/i) or text.match(/girlfriend/i) or text.match(/gf/i)
      msg.send katieRules.join('\n')
    else
      msg.send worfRules.join('\n')
